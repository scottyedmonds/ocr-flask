# OCR-Flask

Using PyTesseract to OCR image. Web App delivered via [Flask](https://www.fullstackpython.com/flask.html) running in Azure Cloud. 

## Requirments
* Ubuntu 18.04 Instance - I used an Azure Standard B1s which is a little overkill
* Python 3.7
* pip
* pipenv
* Python-Tesseract



## Inspiration
McDonalds Monopoly pieces have a 12 digit value that needs to be inputted to web app (mcdpromotion.ca) in order to record an entry for the *Enter Codes and Earn Amazing Rewards!* part of the contest. Typing them in manually got annoying so I did some searching and came up with this. 


-----------------------
Sources:
* https://stackabuse.com/pytesseract-simple-python-optical-character-recognition/
* https://pypi.org/project/pytesseract/